import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public Tnode(String n, Tnode f, Tnode s) {
      name = n;
      firstChild = f;
      nextSibling = s;
   }

   public Tnode() {
      firstChild = null;
      nextSibling = null;
   }

   public Tnode (String name) {
      isOperation(name);
      this.name = name;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      Tnode tnode = this;
      if (!Objects.isNull(children())) {
         b.append("(");
         b.append(children());
         b.append(")");
      }
      if (!Objects.isNull(tnode.nextSibling)) {
         b.append(",");
         b.append(tnode.nextSibling);

      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      String[] nodes = pol.split(" ");
      Stack<Tnode> nodeStack = new Stack<>();
      try {
         for (String node: nodes) {
            nodeStack.push(new Tnode(node));
         }
      } catch (IllegalArgumentException e) {
         throw new IllegalArgumentException("Input expression has invalid characters: " + pol);
      }catch (IndexOutOfBoundsException e) {
         throw new IllegalArgumentException("Input expression has too few numbers: " + pol);
      }
      Tnode root = buildFromStack(nodeStack.pop(), nodeStack);
      if (!nodeStack.empty()) throw new IllegalArgumentException("Input expression has too many numbers: " + pol);
      return root;
   }

   public static Tnode buildFromStack (Tnode root, Stack<Tnode> pol) {
      if (isOperation(root.name)) {
         if (!(root.firstChild == null)) return root;
         if (pol.size() < 2) {
            throw new IndexOutOfBoundsException("Too few nodes in stack: " + pol);
         }
         Tnode second = buildFromStack(pol.pop(), pol);
         Tnode first = buildFromStack(pol.pop(), pol);
         root.firstChild = first;
         first.nextSibling = second;
      } else if (isSpecOp(root.name)) {
         switch (root.name.toLowerCase()) {
            case "dup":
               if (pol.size() < 1) {
                  throw new IndexOutOfBoundsException("Too few nodes in stack, need at least 1: " + pol);
               }
               Stack<Tnode> duplicat = duplicate(pol);
               Tnode first = buildFromStack(duplicat.pop(), duplicat);
               root = first;
               break;
            case "swap":
               if (pol.size() < 2) {
                  throw new IndexOutOfBoundsException("Too few nodes in stack, need at least 2: " + pol);
               }
               first = buildFromStack(pol.pop(), pol);
               Tnode second = buildFromStack(pol.pop(), pol);
               pol.push(first);
               root = second;
               break;
            case "rot":
               if (pol.size() < 3) {
                  throw new IndexOutOfBoundsException("Too few nodes in stack, need at least 3: " + pol);
               }
               first = buildFromStack(pol.pop(), pol);
               second = buildFromStack(pol.pop(), pol);
               Tnode third = buildFromStack(pol.pop(), pol);
               root = third;
               pol.push(second);
               pol.push(first);
         }
      }
      return root;
   }

   private static Stack<Tnode> duplicate(Stack<Tnode> pol) {
      Stack<Tnode> clone = new Stack<>();
      for (Tnode node: pol) {
         Tnode copy = new Tnode(node.name);
         clone.push(copy);
      }
      return clone;
   }

   private Tnode children() {
      return firstChild;
   }

   private static boolean isLeaf(String l) {
      try {
         Integer.parseInt(l);
         return true;
      } catch (Exception e) {
         return false;
      }
   }

   private static String nodesToString(String[] n) {
      if (n.length == 2) return n[0];
      StringBuilder result = new StringBuilder();
      for (int i = 0; i < n.length-1; i++) {
         result.append(n[i]).append(" ");
      }
      return result.toString();
   }

   private static boolean isOperation(String s) {
      boolean result = s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/");
      if (!result && !isLeaf(s) && !isSpecOp(s)) {
         throw new IllegalArgumentException("Input not an acceptable operation: " + s);
      }
      return result;
   }

   private static boolean isSpecOp(String s) {
      return s.toUpperCase().equals("SWAP") || s.toUpperCase().equals("ROT") || s.toUpperCase().equals("DUP");
   }

   public static Tnode createTree() {
      Tnode root = new Tnode ("+",
              new Tnode("1", null, new Tnode("2", null, null)), null);
      return root;
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);

      System.out.println(createTree());

      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }
}