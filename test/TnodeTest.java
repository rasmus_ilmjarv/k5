
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Stack;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
      s = "2 5 9 - +";
      t = Tnode.buildFromRPN(s);
      r = t.toString();
      assertEquals("Tree: " + s, "+(2,-(5,9))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
      System.out.println(root);
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
         Tnode t = Tnode.buildFromRPN ("2 -");
      }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }

   @Test
   public void TestDup() {
      String s = "3 DUP *";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "*(3,3)", r);
      s = "3 5 + DUP *";
      t = Tnode.buildFromRPN(s);
      r = t.toString();
      assertEquals ("Tree: " + s, "*(+(3,5),+(3,5))", r);
   }

   @Test
   public void testSwap() {
      String s = "2 5 SWAP -";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      String expected ="-(5,2)";
      assertEquals ("Tree: " + s, expected, r);
      s = "3 105 + 5 2 - SWAP /";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      expected = "/(-(5,2),+(3,105))";
      assertEquals ("Tree: " + s, expected, r);
      s = "3 105 SWAP + 5 2 - SWAP /";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      expected = "/(-(5,2),+(105,3))";
      assertEquals ("Tree: " + s, expected, r);
   }

   @Test
   public void testRot() {
      String s = "2 5 9 ROT - +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      String expected = "+(5,-(9,2))";
      assertEquals ("Tree: " + s, expected, r);
      s = "2 5 DUP ROT - + DUP *";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      expected = "*(+(5,-(5,2)),+(5,-(5,2)))";
      assertEquals ("Tree: " + s, expected, r);
   }

   @Test
   public void testCombos() {
      String s = "2 5 9 ROT + SWAP -";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      String expected = "-(+(9,2),5)";
      assertEquals ("Tree: " + s, expected, r);
      System.out.println("\n\n\n\n\n\n\n\n");
      s = "2 5 DUP ROT - + DUP *";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      expected = "*(+(5,-(5,2)),+(5,-(5,2)))";
      assertEquals ("Tree: " + s, expected, r);
      s = "-3 -5 -7 ROT - SWAP DUP * +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      expected = "+(-(-7,-3),*(-5,-5))";
      assertEquals ("Tree: " + s, expected, r);
   }
}

